package com.nespresso.sofa.interview.cart.model;

import static java.util.UUID.randomUUID;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class Cart implements Serializable {

    private final UUID id;
    private Map<String, Integer> products;

    public Cart() {
        this(randomUUID());
    }

    public Cart(UUID id) {
        this.id = id;
        this.products = new HashMap<String, Integer>();
    }

    public Cart(Map<String, Integer> products) {
        this.id = randomUUID();
        this.products = products;
    }

    public UUID getId() {
        return id;
    }

    public Map<String, Integer> getProducts() {
        return this.products;
    }

	@Override
	public String toString() {
		return "Cart {id: " + this.id + ", products: "+printProducts()+"}";
	}

	private String printProducts() {
		StringBuffer productsDescription = new StringBuffer();
		for (Map.Entry<String, Integer> entry : this.products.entrySet()) {
			productsDescription.append("{"+entry.getKey()+"="+String.valueOf(entry.getValue())+"}");
		}
		return productsDescription.toString();
	}

	public void addProduct(String productCode, int quantity) {
		 this.products.put(productCode, quantity);
	}


}
