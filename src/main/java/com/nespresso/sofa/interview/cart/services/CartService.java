package com.nespresso.sofa.interview.cart.services;

import java.util.HashMap;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.nespresso.sofa.interview.cart.model.Cart;

public class CartService {

    @Autowired
    private PromotionEngine promotionEngine;

    @Autowired
    private CartStorage cartStorage;
    
    /**
     * Add a quantity of a product to the cart and store the cart
     *
     * @param cartId
     *     The cart ID
     * @param productCode
     *     The product code
     * @param quantity
     *     Quantity must be added
     * @return True if card has been modified
     */
    public boolean add(UUID cartId, String productCode, int quantity) {
    	final Cart cart = cartStorage.loadCart(cartId);
    		if(quantity<=0) return false;
    		Integer oldQuantity = (cart.getProducts().get(productCode)==null)?0:cart.getProducts().get(productCode);
    		cart.addProduct(productCode,quantity+oldQuantity);
    		cartStorage.saveCart(cart);
    		return true;
    }

    /**
     * Set a quantity of a product to the cart and store the cart
     *
     * @param cartId
     *     The cart ID
     * @param productCode
     *     The product code
     * @param quantity
     *     The new quantity
     * @return True if card has been modified
     */
    public boolean set(UUID cartId, String productCode, int quantity) {
    	final Cart cart = cartStorage.loadCart(cartId);
    	Integer oldQuantity = (cart.getProducts().get(productCode)==null)?0:cart.getProducts().get(productCode);
    		if(oldQuantity==quantity) return false;
    		else if(quantity==0){
    			cart.getProducts().remove(productCode);
    			return false;
    		}
    		else if(quantity<0){
    			cart.getProducts().remove(productCode);
    			return true;
    		}
    		else{
    			cart.addProduct(productCode,quantity);
        		cartStorage.saveCart(cart);
        		return true;
    		}
    		
    	
    }

    /**
     * Return the card with the corresponding ID
     *
     * @param cartId
     * @return
     */
    public Cart get(UUID cartId) {
        return cartStorage.loadCart(cartId);
    }
}
